# petanque-gym

A simplified petanque game to be used with OpenAI's Gymnasium to explore strategies with Reinforcement Learning.

## Getting started

## Usage
Keys:
* q - quit
* s - shoot
* p - point
* n - start next end
* r - restart game

## Roadmap
* stagger y pos of boules
* display how to play at start
* opponent can shoot
* conform to Gymnasium's environment interface.
* add observation space - https://www.gymlibrary.dev/content/environment_creation/
* add carreaux
* [ANSI rendering]

## License
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <john.crane@gmail.com> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. 
 * ----------------------------------------------------------------------------

## Project status
Active
