'''
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <john.crane@gmail.com> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. 
 * ----------------------------------------------------------------------------
'''

import numpy as np
import cv2
import random
import time
from typing import NamedTuple, List, Final
from enum import Enum

class Action(Enum):
    POINT = 0
    SHOOT = 1

PLYR = 0 # player
OPPT = 1 # opponent

MAX_DISTANCE: Final = 99
NUM_BOULES: Final = 6
OUT_OF_BOUNDS: Final = MAX_DISTANCE + 2
PLAY_TO: Final = 13

class PlayState(Enum):
    IN_HAND = 0
    IN_PLAY = 1
    OOB = 2

class Boule():

    def __init__(self, player : int = PLYR) -> None:
        self.state = PlayState.IN_HAND
        self.owner = player
        self.distance = MAX_DISTANCE


def inPlay(l : List[Boule]) -> List[Boule]:
    return [b for b in l if b.state == PlayState.IN_PLAY]

def inHand(l : List[Boule]) -> List[Boule]:
    return [b for b in l if b.state == PlayState.IN_HAND]

def ownedBy(o : int, l : List[Boule]) -> List[Boule]:
    return [b for b in l if b.owner == o]

class GameState():    
    def __init__(self) -> None:
        pl = [Boule(PLYR) for i in range(NUM_BOULES)]
        ol = [Boule(OPPT) for i in range(NUM_BOULES)]
        self.boules = pl + ol


    def closestBoule(self) -> Boule:
        return self.__closest(self.boules)


    def closestBouleOf(self, owner : int) -> Boule:
        return self.__closest(ownedBy(owner, self.boules))


    def __closest(self, l : List[Boule]) -> Boule:
        assert len(l) > 0
        closest = l[0]
        for b in l:
            if b.distance < closest.distance:
                closest = b
        return closest


    # remove the closest opponent boule from play
    def __removeShotBoule(self, player : int):
        b = self.closestBouleOf(player)
        if b.state == PlayState.IN_PLAY:
            b.state = PlayState.OOB
            b.distance = MAX_DISTANCE
    
    def countBoulesInside(self, player : int, dist: int) -> int:
        points = 0
        for d in inPlay(ownedBy(player, self.boules)):
            if d.distance < dist:
                points += 1
        return points
    

    def isEndOver(self) -> bool:
        return len(inHand(self.boules)) == 0


    # attempt to toss boule close to jack     
    def point(self, p : int = PLYR):
        b = None
        for b in inHand(ownedBy(p, self.boules)):
            b.state = PlayState.IN_PLAY
            b.distance = random.randint(0,MAX_DISTANCE)

            # don't allow duplicate distances
            for bb in inPlay(self.boules):
                if b.distance == bb.distance:
                    b.distance += 1
            break
        assert b != None, "Tried to point without boules"


    # update player after shooting, return if successful   
    # accuracy - float between 0.0 and 1.0 specifies how often shooting will be successful
    def shoot(self, p : int = PLYR, accuracy : float = 0.33):
        b = None
        for b in inHand(ownedBy(p, self.boules)):
            b.state = PlayState.OOB
            if (random.uniform(0.0, 1.0) < accuracy):
                self.__removeShotBoule(PLYR if p == OPPT else OPPT)
            break
        assert b != None, "Tried to shoot without boules"

class Observation(NamedTuple):
    gameState: GameState
    playerScore: int
    opponentScore: int

        
# A custom Gymnasium environment to play Petanque 
class PetanqueEnv():
    img: np.ndarray = None
    jackPos: List[int] = []
    metadata = {"render_modes": ["human"], "render_fps": 2}


    def __init__(self) -> None:
        self.img = np.ones((300, 800, 3),dtype='uint8')
        self.scores = [0, 0]
        self.gameState = GameState()


    # return the player who has the boule closest to the jack
    def closestPlayer(self) -> int:
        return self.gameState.closestBoule().owner


    def __createObservation(self) -> Observation:
        return Observation(
                self.gameState,
                self.scores[PLYR],
                self.scores[OPPT]
        )


    def __isEndOver(self) -> bool:
        return self.gameState.isEndOver()


    def __renderBoulesInPlay(self, yOffsets, colors):
        boules = inPlay(self.gameState.boules)
        boules.sort(key=lambda b: b.distance) #reduces jumping
        stagger = 0
        for b in boules:
            cv2.circle(self.img, (self.jackPos[0] - b.distance * 5 ,self.jackPos[1]+yOffsets[b.owner]+stagger), 10, (255,255,255), -1)
            cv2.circle(self.img, (self.jackPos[0] - b.distance * 5 ,self.jackPos[1]+yOffsets[b.owner]+stagger), 9, colors[b.owner], -1)
            stagger += 4


    def __renderBoulesOutOfBounds(self, yOffsets, colors):
        obCount = 0
        stagger = 0
        boules = [b for b in self.gameState.boules if b.state == PlayState.OOB]
        for b in boules:
            (bl,g,r) = colors[b.owner]
            cv2.circle(self.img, (50 + obCount*30, self.jackPos[1]+2*yOffsets[b.owner]), 9, (bl/2,g/2,r/2), 3)
            obCount += 1

    def __resetBoules(self):
        self.gameState = GameState()


    # calculate the winning score and return True if the game is over
    def __updateScores(self) -> bool:
        winner = self.closestPlayer()
        loser = PLYR if winner == OPPT else OPPT 

        # find closest boule of loser, if any in play, and count scoring boules of winner
        dist = self.gameState.closestBouleOf(loser).distance
        points = self.gameState.countBoulesInside(winner, dist)
        wScore = self.scores[winner] + points
        lScore = self.scores[loser]

        self.scores[winner] = wScore
        self.scores[loser] = lScore

        return wScore >= PLAY_TO
    
                
    def close(self):
        cv2.destroyAllWindows()


    def init(self):
        self.jackPos = [700, 160]
        self.reset()


    def render(self):
        cv2.imshow('a',self.img)
        cv2.waitKey(1)
        self.img = np.zeros((300,800,3),dtype='uint8')

        # Display jack
        cv2.rectangle(self.img,(self.jackPos[0],self.jackPos[1]),(self.jackPos[0]+10,self.jackPos[1]+10),(0,0,255),3)

        scoreText = f"Score {self.scores[0]} / {self.scores[1]}"
        
        if self.__isEndOver():
            scoreText = scoreText + " - End completed."
            cv2.putText(self.img, scoreText, (50,50), cv2.FONT_HERSHEY_DUPLEX, 1, (99,99,255))
        else:
            cv2.putText(self.img, scoreText, (50,50), cv2.FONT_HERSHEY_DUPLEX, 1, (199,99,99))
        
        self.__renderBoulesInPlay([-40, 40], [(0,255,0), (255,0,0)])
        self.__renderBoulesOutOfBounds([-40, 40], [(0,255,0), (255,0,0)])
        

    def reset(self):
        self.__resetBoules()
        self.scores = [0, 0]


    # return true if game is over
    def step(self, action) -> (Observation, bool):
        # if end is over, reset boules
        if self.__isEndOver():
            self.__resetBoules() 

        if action == Action.POINT:
            self.gameState.point(PLYR)
        elif action == Action.SHOOT:
            self.gameState.shoot(PLYR)
        else:
            assert False, "invalid action"

        # Opponent will point if not closest and has boules, or if PLYR is out of boules
        while (self.closestPlayer() == PLYR or len(inHand(ownedBy(PLYR, self.gameState.boules))) == 0) and len(inHand(ownedBy(OPPT, self.gameState.boules))) > 0:
            self.gameState.point(OPPT)

        # if end is over, update scores
        if self.__isEndOver():
            self.__updateScores() 

        if self.scores[PLYR] > PLAY_TO or self.scores[OPPT] > PLAY_TO:
            return (self.__createObservation(), True)
        return (self.__createObservation(), False)
    

def main():
    pEnv = PetanqueEnv()
    pEnv.init()

    gameOver = False
    while True:
        pEnv.render()

        # Takes step after fixed time
        t_end = time.time() + 0.05
        k = -1
        while time.time() < t_end:
            if k == -1:
                k = cv2.waitKey(1)
            else:
                continue
                
        if k == ord('s'):
            (obs, gameOver) = pEnv.step(Action.SHOOT)
        elif k == ord('p'):
            (obs, gameOver) = pEnv.step(Action.POINT)
        elif k == ord('r'):
            pEnv.reset()
            gameOver = False
        elif k == ord('q'):
            break
        else:
            pass
            
    pEnv.close()
    
if __name__ == "__main__":
	main()
